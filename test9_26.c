#define _CRT_SECURE_NO_WARNINGS
//32456684
//把一个数的各个位数相加，直到这个和为个位数就打印
#include <stdio.h>
//int main() {
//    int n = 0;
//    int sum = 0;
//    int a = 0;
//    scanf("%d", &n);
//    do {
//        a = n;
//     while (a)
//     {
//        int ret = a% 10;
//        a /= 10;
//        sum += ret;
//     }
//        if (sum > 9)
//        {
//            n = sum;
//            sum = 0;
//        }
//        else 
//        {
//            n = 0;
//        }
//    } while (n > 9);
//    printf("%d", sum);
//    return 0;
//
//}
//更简单的代码；

//#include <stdio.h>
//int main() {
//    int n = 0;
//    int sum = 0;
//    int a = 0;
//    scanf("%d", &n);
//    do {           //至少循环一次
//        sum = 0;
//        while (n)
//        {
//            int ret = 0;
//            ret = n % 10;
//            n /= 10;
//            sum += ret;
//        }
//        if (sum > 9)//这里判断一下，如果sum还是两位数的话，再循环；
//        {
//            n = sum;//这里把sum的值赋给n，使n的值满足循环条件；
//        }
//    } while (n>9);
//    printf("%d", sum);
//    return 0;
//
//}

//请统计某个给定范围[L, R]的所有整数中，数字2出现的次数。
//
//比如给定范围[2, 22]，数字2在数2中出现了1次，在数12中出现1次，在数20中出现1次，
//在数21中出现1次，在数22中出现2次，所以数字2在该范围内一共出现了6次。
//#include <stdio.h>
//
//int main() {
//    int L = 0;
//    int R = 0;
//    scanf("%d %d", &L, &R);
//    int i = 0;
//    int count = 0;
//    for (i = L; i <= R; i++)
//    {
//        int a = i;
//        while (a)
//        {
//            int ret = a % 10;
//            a /= 10;
//            if (ret == 2)
//            {
//                count++;
//            }
//        }
//    }
//    printf("%d\n", count);
//    return 0;
//}
//函数的做法；
//#include <stdio.h>
//int count_2(int a,int b)
//{
//    while (a)
//    {
//        int ret = a % 10;
//        a /= 10;
//        if (ret == 2)
//        {
//            b++;
//        }   
//    }
//    return b;
//}
//int main()
//{
//    int L = 0;
//    int R = 0;
//    int count = 0;
//    int a = 0;
//    scanf("%d %d", &L, &R);
//    int i = 0;
//    for (i = L; i <= R; i++)
//    {
//         a += count_2(i,count);
//    }
//    printf("%d\n", a);
//    return 0;
//}


//牛牛很喜欢发短信，他所用的通讯公司的收费方法如下：
//1.每条短信收费0.1元
//2.每条短息的字数不超过60字，如果超过60字，将按照0.2元的价格收费。
//牛牛这个月发了 n 条短信，请问牛牛一共要缴多少短信费
#include <stdio.h>

//int main() {
//    int n = 0;
//    scanf("%d", &n);
//    int a = 0;
//    double sum = 0;
//    while (n)
//    {
//        scanf("%d", &a);
//        if (a <= 60)
//        {
//            sum += 0.1;
//        }
//        else
//        {
//            sum += 0.2;
//        }
//        n--;
//    }
//    printf("%.1lf", sum);
//    return 0;
//}


//牛牛刚刚学了素数的定义：素数值指在大于1的自然数中，除了1和它本身以外不再有其他因数的自然数
//牛牛想知道在[l, r] 范围内全部素数的和
//#include <stdio.h>
//
//int main() {
//    int l = 0;
//    int r = 0;
//    scanf("%d %d", &l, &r);
//    int i = 0;
//    int sum = 0;
//    for (i = l; i <= r; i++)
//    {
//        int a = i;
//        int j = 0;
//        for (j = 2; j < a; j++)
//        {
//            int ret = a % j;
//            if (ret == 0)//等于零说明不是素数，直接跳出循环
//            {
//                break;
//            }
//            if (j == a - 1)//这里说明a没有找到一个除了1，和他本身可以整除的数，是素数
//            {
//                sum += a;
//            }
//        }
//    }
//    printf("%d", sum);
//        return 0;
//}

//错误代码
#include <stdio.h>

//int main()
//{
//    int n = 0;
//    int m = 0;
//    int a[100] = { 0 };
//    int b[100] = { 0 };
//    int k, l;
//    int sumb = 0; int suma = 0;
//    scanf("%d %d", n, m);
//    int i = 0;
//    for (i = 0; i < n; i++)
//    {
//        scanf("%d", &a[i]);
//        suma += a[i];
//    }
//    for (i = 0; i < m; i++)
//    {
//        scanf("%d", b[i]);
//    }
//    for (i = 0; i < m; i++)
//    {
//        int j = 0;
//        for (j = i + 1; j < m - 1 - i; j++)
//        {
//            b[i] += b[j];
//            if (b[i] == suma)
//            {
//                int ret = b[i] - sumb;
//                if (ret < 0)
//                {
//                    sumb = b[i];
//                    int k = i;
//                    int l = j;
//                }
//            }
//        }
//    }
//    for (int i = k; i < l; i++)
//    {
//        printf("%d", b[i]);
//    }
//    return 0;
//}
//牛牛刚学会数组不久，他拿到两个数组 a 和 b，询问 b 的哪一段连续子数组之和与数组 a 之和最接近。
//如果有多个子数组之和同样接近，输出起始点最靠左的数组。
//2 6
//30 39
//15 29 42 1 44 1
//29 42
#include <stdio.h>
int num(int a, int b)
{
	if (a >= b)
		return a - b;
	else
		return b - a;
}
int main()
{
	int n, m;
	scanf("%d %d", &n, &m);
	int a[100] = { 0 };
	int b[100] = { 0 };
	int sum1 = 0, sum2 = 0, min, k, l;
	for (int i = 0; i < n; i++)
	{
		scanf("%d", &a[i]);
	}
	for (int i = 0; i < m; i++)
	{
		scanf("%d", &b[i]);
	}
	for (int i = 0; i < n; i++)
	{
		sum1 += a[i];
	}
	min = sum1;
	for (int i = 0; i < m; i++)
	{
		sum2 = b[i];
		for (int j = i + 1; j <= m; j++)
		{
			if (num(sum1, sum2) < min)
			{
				min = num(sum1, sum2);
				k = i;
				l = j;
			}
			sum2 += b[j];

		}
	}
	for (int i = k; i < l; i++)
	{
		printf("%d ", b[i]);
	}

	return 0;
}