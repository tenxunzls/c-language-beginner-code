#define _CRT_SECURE_NO_WARNINGS
//小q最近迷上了各种好玩的数列，这天，他发现了一个有趣的数列，其递推公式如下：
//
//f[0] = 0 f[1] = 1;
//f[i] = f[i / 2] + f[i % 2]; (i >= 2)
//
//现在，他想考考你，问：给你一个n，代表数列的第n项，你能不能马上说出f[n]的值是多少，
//以及f[n]所代表的值第一次出现在数列的哪一项中？
//（这里的意思是：可以发现这个数列里某几项的值是可能相等的，
//则存在这样一个关系f[n'] = f[n] = f[x/2]+f[x%2] = f[x]...（n' < n < x）
//他们的值都相等，这里需要你输出最小的那个n'的值)(n<10^18)
//#include <stdio.h>

//int fn(int x)//计算f(n)
//{
//    if (x >= 2)
//    {
//        int n = x;
//        x = fn(n / 2) + fn(n % 2);
//    }
//    return x;
//}
//int fn1(int x, int y)//计算等于f(n)的最小的那个n;
//{
//    int i = 0;
//    for (i = x; i <= y; i++)
//    {
//        if (fn(i) == x)
//        {
//            break;
//        }
//    }
//    return i;
//}
//int main()
//{
//    int t = 0;
//    scanf("%d", &t);
//    int i = 0;
//    int n = 0;
//    for (i = 0; i < t; i++)
//    {
//        scanf("%d", &n);
//        printf("%d %d\n", fn(n), fn1(fn(n), n));
//    }
//    return 0;
//}
//
////这是大佬的代码，大佬看问题就是不一样，我终究还是个小白，还有很长的路要走；
///*f(n)结果就是对应n二进制中1的个数，n’就是最早出现对应个数1的那个;
// (110 101 1010....都是两个1, 结果都是2，最早的应该是二进制为11);
//eg : f(n) = 1 最早满足的就是二进制 1 ---- n' = 1    (2 ^ 1 - 1);
//     f(n) = 2 最早满足的就是二进制11 ---- n' = 3    (2 ^ 2 - 1);
//---> f(n) = x 最早满足的就是二进制为x个全1 ---- n' = (2 ^ x - 1);
//*/
//#include <stdio.h>
//#include <math.h>
//
//int main() {
//    int t, count;
//    long n;
//    scanf("%d", &t);
//    for (int i = 0; i < t; i++) {
//        scanf("%ld", &n);
//        count = 0;
//        while (n) {
//            count += n & 1 == 1 ? 1 : 0; //对应二进制1的个数  
//            n >>= 1;
//        }
//        printf("%d %ld\n", count, (long)pow(2, count) - 1);
//    }
//    return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    getchar();
//    char a[4] = { 0 };
//    int i = 0;
//    while (i < 4)
//    {
//        scanf("%c", &a[i]);
//        getchar();
//        i++;
//    }
//    char arr[100] = { 0 };
//    i = 0;
//    for (i = 0; i < n; i++)
//    {
//        scanf("%c", &arr[i]);
//        arr[i] = a[i];
//        printf("%c", arr[i]);
//    }
//
//    return 0;
//}
//
////第一行输入一个正整数 n 表示字符数组的长度，四个个字符分别 a1 和 a2 ， a3 和 a4，
//// 表示把字符数组中 a1 全部替换成 a2，然后把 a3 全部替换成 a4（包括a1替换后产生的a2等于a3的情况）
////第二行输入一个长度为 n 的字符数组。
//#include <stdio.h>
//
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    getchar();//吸取空格
//    char a[4] = { 0 };
//    int i = 0;
//    while (i < 4)
//    {
//        scanf("%c", &a[i]);
//        getchar();//吸取空格
//        i++;
//    }
//    char arr[100] = { 0 };
//    i = 0;
//    for (i = 0; i < n; i++)
//    {
//        scanf("%c", &arr[i]);
//        if (arr[i] == a[0]) arr[i] = a[1];
//        if (arr[i] == a[2]) arr[i] = a[3];
//        printf("%c", arr[i]);
//    }
//
//    return 0;
//}
//
//
////牛牛刚刚学了素数的定义：素数值指在大于1的自然数中，除了1和它本身以外不再有其他因数的自然数
////牛牛想知道输入的 n 个数分别是不是素数
////输入描述：
////第一行输入一个正整数 n ，表示后续要输入的数的数量。
////后续 n 行每行输入一个正整数，表示需要判断的数。
//#include <stdio.h>
//
//int main()
//
//{
//    int n = 0;
//    scanf("%d", &n);
//    int a = 0;
//    while (n)
//    {
//        scanf("%d", &a);
//        n--;
//        int i = 0;
//        for (i = 2; i < a; i++)
//        {
//            if (a % i == 0)
//            {
//                printf("false\n"); break;
//            }
//        }
//        if (i == a) printf("true\n");
//    }
//
//    return 0;
//}