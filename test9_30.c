#define _CRT_SECURE_NO_WARNINGS

//牛牛最近学了水仙花数，但是他并不喜欢水仙花，因此他准备算出[l, r] 区间内的四叶玫瑰数。
//四叶玫瑰数：一个数的四个位置的数字的四次方加起来等于这个四位数本身的数。
//输入描述：
//第一行输入两个正整数，表示闭区间的两头
//输出描述：
//输出区间内的四叶玫瑰数，保证至少有一个
#include <stdio.h>
#include <math.h>

int main()
{
    int a, b;
    scanf("%d %d", &a, &b);
    int i = 0;
    for (i = a; i <= b; i+=1633)
    {
        int sum = 0;
        int j = i;
        while (j)
        {
            int ret = j % 10;
            j /= 10;
            sum += (int)pow(ret, 4);
        }
        if (sum == i)
        {
            printf("%d ", i);
        }
    }

    return 0;
}
