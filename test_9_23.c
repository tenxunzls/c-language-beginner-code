//用函数指针数组编写一个计算器
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
void mum()
{
	printf("**********************\n");
	printf("**1.加法     2.减法 **\n");
	printf("**3.乘法     4.除法 **\n");
	printf("**     0.退出       **\n");
	printf("**********************\n");
}
int Add(int x, int y)//加法函数
{
	return x + y;
}
int Sub(int x, int y)//减法函数
{
	return x - y;
}
int Mul(int x, int y)//乘法函数
{
	return x * y;
}
int Div(int x, int y)//除法函数
{
	return x / y;
}
int main()
{
	
	int input = 0;
		
	do {
		mum();//选择菜单
		printf("请选择\n");
		scanf("%d", &input);
		
		if (input >= 1 && input <= 4)
		{
			//把函数存在一个函数指针数组arr里。
			int (*arr[])(int, int) = { 0,Add,Sub,Mul,Div };
			int a = 0;
			int b = 0;
			printf("请输入两个操作数\n");
			scanf("%d%d", &a, &b);
			int ret = arr[input](a, b);
			
			printf("%d\n", ret);
		}
		else if (input == 0)
		{
			printf("退出\n");
		}
		else {
			printf("选择错误\n");
		}

	} while (input);
		return 0;
}