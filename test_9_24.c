#define _CRT_SECURE_NO_WARNINGS
//定义一个通用的将任意数组内元素排成升序的my_sqort函数，利用回调函数知识。
//该函数还有待完善，两个浮点型的元素之差如果在0到1之间的话，return一个正形就会丢失精度
#include <stdio.h>
int comp_int(char* x, char* y)//整形元素比较函数，比较两个整形元素的大小。
{
	return *(int*)x - *(int*)y;
}
int comp_double(char* x, char* y)//浮点型元素比较大小
{
	return *(double*)x - *(double*)y;
}
int comp_char(char* x, char* y)//字符型元素比较大小
{
  return *x - *y;
}
void swap(char* a, char* b, int width)//交换函数，负责交换两个元素的位置，排成升序。
{
	int i = 0;//一个字节一个字节地交换，如果传过来的元素是Int，则交换4个字节，即循环四次
	for (i = 0; i < width; i++)
	{
		char tmp = 0;
		tmp = *a;
		*a = *b;
		*b = tmp;
		a++;
		b++;
	}
	
}
void my_qsort(void* start, int sz, int width, int (*p)(void* e1, void* e2))//调用回调函数
{
	int i = 0;
	{
		for (i = 0; i < sz - 1; i++)
		{
			int j = 0;
			for (j = 0; j < sz - 1 - i; j++)
			{
				//先比较
				//将传过来的数组元素的指针强制转化为char*,就能一个字节一个字节地访问元素了，方便后面一个字节一个字节的交换元素。
				if (p((char*)start + j * width, (char*)start + (j + 1) * width) > 0)
				{
					//再交换
					swap((char*)start + j * width, (char*)start + (j + 1) * width, width);
				}

			}
		}
	}

}
void test1()//整形数组
{
	int arr[] = { 9,8,7,6,6,2,0,1,5,4 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int width = sizeof(arr[0]);
	my_qsort(arr, sz, width, comp_int);
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);

	}
	printf("\n");
	
}
void test2()//浮点型数组
{
	double arr[] = { 9.0,8.0,7.0,6.0,3.0,2.0,0.0,1.0,5.0,4.0 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int width = sizeof(arr[0]);
	my_qsort(arr, sz, width, comp_double);
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%.1lf  ", arr[i]);

	}
	printf("\n");
}
void test3()//字符形数组
{
	char arr[] = { "dkjnipgoea"};
	int sz = sizeof(arr) / sizeof(arr[0]);
	int width = sizeof(arr[0]);
	my_qsort(arr, sz, width, comp_char);
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%c ", arr[i]);

	}
	printf("\n");

}

int main()
{
	test1();
	test2();
	test3();
   return 0;
}
