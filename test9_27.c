#define _CRT_SECURE_NO_WARNINGS
//现在给出一个素数，这个素数满足两点：
//
//1、  只由1 - 9组成，并且每个数只出现一次，如13, 23, 1289。
//
//2、  位数从高到低为递减或递增，如2459，87631。
//
//请你判断一下，这个素数的回文数是否为素数（13的回文数是131, 127的回文数是12721）。
//#include <stdio.h>
//#include <math.h>
//int jgde(int x)
//{
//    int i = 0;
//    int isprime = 0;
//    for (i = 2; i < x; i++)
//    {
//        int ret = x % i;
//        if (ret == 0)break;
//        isprime = 1;
//    }
//    return isprime;
//}
//int jgde_n(int x)
//{
//    int n=0;
//    while(x)
//    {
//        int ret=x%10;
//        x/=10;
//        n++;
//    }
//    return n;
//}
//
//int main()
//{
//    int t = 0;
//     int n=0;
//    scanf("%d", &t);
//    int i = 0;
//    int a = t;
//    n = jgde_n(t);
//   t *= pow(10, (double)n - 1);
//    while (a)
//    {
//        int ret = a % 10;
//        a /= 10;
//        if (i > 0)t += ret * pow(10, (double)n - 1 - i);
//        i++;
//    }
//    printf("%d\n", t);
//    if (jgde(t) == 1)printf("prime");
//    else printf("noprime");
//    return 0;
//}
//#include <stdio.h>
//#include <math.h>
//int jgde(long long x)//这个函数用来判断是否为素数判断；
//{
//    long long i = 0;
//    int  isprime = 0;
//    for (i = 2; i < x; i++)
//    {
//        long long ret = x % i;
//        if (ret == 0)break;
//        if(i==x-1)
//        isprime = 1;
//    }
//    return isprime;
//}
//int jgde_n(long long x)//这个函数判断一下t是几位数，位数n计算回文数要用；
//{
//    int n = 0;
//    while (x)
//    {
//        long long ret = x % 10;
//        x /= 10;
//        n++;
//    }
//    return n;
//}
//long long change_t(long long x, int y)//这个函数用来计算t的回文数；
//{
//    long long a = x;
//    int i = 0;
//    x *= pow(10, (double)y - 1);
//    while (a)
//    {
//        long long ret = a % 10;
//        a /= 10;
//        if (i > 0) x += ret * pow(10, (double)y - 1 - i);
//        i++;
//    }
//    return x;
//}
//
//int main()
//{
//    long long t = 0;
//    int n = 0;
//    scanf("%lld", &t);
//    long long a = t;
//    n = jgde_n(t);
//    t = change_t(t, n);
//    printf("%lld\n", t);
//    if (jgde(t)==1)printf("prime");
//    else printf("noprime");
//    return 0;
//}
//最终版
//测试的时候发现12456789这么多位的数字int存不下，所以换成longlong
//#include <stdio.h>
//#include <math.h>
//int jgde(long long x)//这个函数用来判断是否为素数判断；
//{
//    long long i = 0;
//    int isprime = 0;
//    for (i = 2; i < sqrt(x); i++)
//    {
//        isprime = 1;
//        int ret = x % i;
//        if (ret == 0)
//        {
//            isprime = 0;
//            break;
//        }
//    }
//    return isprime;
//}
//int jgde_n(long long x)//这个函数判断一下t是几位数，位数n计算回文数要用；
//{
//    int n = 0;
//    while (x)
//    {
//        long long ret = x % 10;
//        x /= 10;
//        n++;
//    }
//    return n;
//}
//long long change_t(long long x, int y)//这个函数用来计算t的回文数；
//{
//    long long a = x;
//    int i = 0;
//    x *= pow(10, (double)y - 1);
//    while (a)
//    {
//        long long ret = a % 10;
//        a /= 10;
//        if (i > 0) x += ret * pow(10, (double)y - 1 - i);//倒数第二位开始，每取出t的一位，就令t+=这个位数的n-1-i次方。
//        i++;
//    }
//    return x;
//}
//
//int main()
//{
//    long long t = 0;
//    int n = 0;
//    scanf("%lld", &t);
//    n = jgde_n(t);
//    t = change_t(t, n);
//    printf("%lld", t);
//    if (jgde(t))printf("prime");
//    else printf("noprime");
//    return 0;
//}

//兔子发现了一个数字序列，于是开始研究这个序列。兔子觉得一个序列应该需要有一个命名，命名应该要与这个序列有关。
//由于兔子十分讨厌完全平方数，所以兔子开创了一个新的命名方式：这个序列中最大的不是完全平方数的数字就是他的名字。
//现在兔子有一个序列，想要知道这个序列的名字是什么。
//#include <stdio.h>
//#include <math.h>
//int max(int x, int y)//比较大小
//{
//    if (x >= y)
//        return x;
//    else return y;
//}
//int  jgde(int x)//判断王权平方数；
//{
//    int i = 0;int flag;
//    double tmp = sqrt(x);
//    for (i = 0; i < tmp + 1; i++)
//    {
//        flag = 0;
//        if ((int)pow(i, 2) == x)
//            flag = 1;
//    }
//    return flag;
//}
//int main() 
//{
//    int arr[1000] = { 0 };
//    int n = 0;
//    scanf("%d", &n);
//    int i = 0;int min = 0;
//    for (i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//        if (jgde(arr[i]) == 0)
//            min = max(arr[i], min);
//    }
//    printf("%d", min);
//
//    return 0;
//}