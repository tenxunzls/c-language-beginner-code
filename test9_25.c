#define _CRT_SECURE_NO_WARNINGS
//将T组四位数进行倒置打印；
//#include <stdio.h>
//int main()
//{
//	int T = 0;
//	scanf("%d", &T);//输入T
//	int i = 0;
//	for(i=0;i<T;i++)//循环输入T组四位数
//	{
//		int x = 0;
//		int j = 0;
//		scanf("%d", &x);//输入4位数
//		for (j = 0; j < 4; j++)//循环4次，依次将4位数倒着打印
//		{
//			int ret = x % 10;
//			x /= 10;
//			printf("%d", ret);
//		}
//		printf("\n");
//	}
//	return 0;
//}
// //简单一组倒置打印
//#include <stdio.h>
//int main()
//{
//	int a;
//	scanf("%d", &a);
//	while (a)
//	{
//		printf("%d", a % 10);
//		a = a / 10;
//	}
//	return 0;
//}
// //输出日期
//#include <stdio.h>
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//	int c = 0;
//	scanf("%d%d%d", &a, &b, &c);
//	printf("%d年| ",a);
//	printf("%d月| ", b);
//	printf("%d日", c);
//
//	return 0;
//}

//实现MAX函数
//#include <stdio.h>
//int max3(int x, int y, int z)
//{
//    int max = x;//比较三个数的大小
//    if (x <= y)
//    {
//        max = y;
//        if (y <= z)
//        {
//            max = z;
//        }
//    }
//    else {
//        if (x <= z)
//            max = z;
//    }
//    return max;
//}
//
//int main() {
//    int a = 0;
//    int b = 0;
//    int c = 0;
//    scanf("%d%d%d", &a, &b, &c);
//    double m = max3(a + b, b, c) / (max3(a, b + c, c) + max3(a, b, b + c) * 1.0);
//    printf("%.2lf\n", m);
//
//    return 0;
//}
/*更简单的代码
#include<stdio.h>

int max3(int a,int b,int c){
    if(a>b) b=a;
    return b>c?b:c;
}

int main(){
    int a,b,c;
    scanf("%d %d %d",&a,&b,&c);
    printf("%.2f",(float)max3(a+b,b,c)/(max3(a,b+c,c)+max3(a,b,b+c)));
}*/

//输出ab区间内各个位数加起来是5的倍数的数有多少个；
#include <stdio.h>

int main() {
    int a = 0;
    int b = 0;
    scanf("%d %d", &a, &b);
    int m = 0;
    int count = 0;
    int i = 0;
    for (i=a; i<= b;i++)//遍历a到b区间的所有数；
    {
        m = i;//这一步很重要，将i的值保存到m里，下面的操作只会改变m,而不会改变i,这样i的值才能遍历a到b区间内所有的数。
        int sum = 0;
        //取出m的所有个位数
        while (m)
        {
            int ret = 0;
            ret=m % 10;//将每个个位数保存起来。
            sum+=ret;//将每个各位数加起来。
            m /= 10;
        }
        if (sum % 5 == 0)//判断和是否为5的倍数
        {
            //printf("%d\n", i);
            count++;
        }
    }
    printf("%d\n", count);
    return 0;
}